var express = require('express');
var app = express();
var port = process.env.PORT || 3541;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');
var configDB = require('./config/database.js');
var server = http.createServer(app)

var chats = require('./routes/chat');
var Chat = require('./models/chatModel.js');


//MONGODB connection test
mongoose.connect(configDB.url, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("CONNECTION HAS BEEN ESTABLISHED!");
    }
});


require('./config/passport')(passport);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {  

    Chat.find({}, function (err, docs) {
        if (err) throw err;
        socket.emit('Get Chat', docs);
    });

    socket.on('create chat', function (data) {

        var newChat = new Chat({
            chat: data.chat,
            chatID: data.chatID,
            username: data.username
        });
        newChat.save(function (err) {
            if (err) throw err;

            io.sockets.emit('Get New Chats', data);
        });
    });

});

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs');

app.use(session({
    secret: 'webtech',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./routes/login')(app, passport);

server.listen(port);
console.log('Server is running on port - ' + port);

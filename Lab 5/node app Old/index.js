var express = require('express');
var app = express();
var http = require('http');
var port     = process.env.PORT || 3341;
var mongoose = require('mongoose');
var swig = require('swig');
var bodyParser = require('body-parser');
var path = require('path');
var restful = require('node-restful');
var io = require('socket.io')(http);
var server = http.createServer(app)

var port = process.env.PORT || 3043;

mongoose.connect('mongodb://localhost/imdChat'); //CONNECT TO DB

app.set('view engine', 'ejs'); // set up ejs for templating

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  	extended: true
}));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.render(__dirname + '/views/index.ejs');
});

var Message = app.message = restful.model('Message', mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}, {
    collection: 'message'
})).methods(['get', 'post', 'put', 'delete']);
Message.register(app, '/message');

io.on('connection', function (socket) {
            console.log('User Connected');
            socket.on('disconnect', function () {
                console.log('User Disconnected');
            });

            socket.on('message', function (a) {
                Message.create({
                    name: a
                }, function (err, b) {
                    console.log('message added:', b);
                    io.emit('update', b);
                });
            });


            io.on('connect', function (socket) {

                Message.find()
                    .exec(function (err, messages) {
                        socket.emit('listMessages', messages);
                    });
            });

});
server.listen(port);
console.log('Server is running on port - ' + port);

module.exports = app;
// access the router object, so that we can add routes to it
var express = require('express');
var router = express.Router();

app.get('/', function(req, res){
  res.render('home.jade');
});

module.exports = router;
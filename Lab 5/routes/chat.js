var express = require('express');
var router = express.Router();
var Topic = require('../models/chatModel.js');

    router.get('/', function(req, res, next) {
        res.render('chat.ejs', { title: 'Topics', user: req.user });
    });
    
    router.get('/id/:id', function(req, res, next) {
        console.log("Sup boi");
        var id = req.params.id;

        Topic.findOne({topicID: id}, function(err, doc){
            console.log("log doc van in de route " + doc);
            res.render('questions.ejs', {docs: doc});
        });
    });

module.exports = router;
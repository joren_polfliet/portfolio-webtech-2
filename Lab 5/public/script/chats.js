$(document).ready(function () {

    //Save info into DB
    var socket = io.connect();
    var $chatForm = $('#create_chat_form');
    var $chatName = $('#chat_name');
    var $username = $('#username');
    var $allchatsDiv = $('#allchats');

    var i = 1;

    $chatForm.submit(function (e) {
        e.preventDefault();

        //send chat to the server
        socket.emit('create chat', {
            chat: $chatName.val(), 
            chatID: i,
            username: $username.val()
        });
        $chatName.val('');
    });

    //receiving older chats from server
    socket.on('Get Chat', function (docs) {
        for (var i = 0; i < docs.length; i++) {
            displayChat(docs[i]);
        }
    });


    //receiving new chats from server
    socket.on('Get New Chats', function (data) {
        displayChat(data);
    });

    
    function displayChat(data) {   
            $allchatsDiv.prepend("<p class='hidden fadenewitem' class='hidden fadenewitem' href='/chat/id/" + data.chatID + "'><div class='listItem' id='chatNew" + i + "'><p>" + data.username + " zegt: " +   data.chat + "</p></div></p>");
            i++;
        
    }
});

var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/login.js');

module.exports = function (passport) {

    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use('local-signup', new LocalStrategy({
            nameField: 'name',
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {

            User.findOne({
                'local.email': email
            }, function (err, user) {

                if (err)
                    return done(err);
                if (user) {
                    return done(null, false, req.flash('signupMessage', 'Je email bestaat al.'));
                } else {

                    var newUser = new User();
                    
                    console.log(req.body.name);
                    newUser.local.name = req.body.name;
                    newUser.local.email = email;
                    newUser.local.password = newUser.generateHash(password);

                    newUser.save(function (err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }

            });

        }));

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            User.findOne({
                'local.email': email
            }, function (err, user) {
                if (err)
                    return done(err);
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'De gebruiker bestaat niet. Maak een account!'));
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Wachtwoord is verkeerd. Probeer het opnieuw!'));
                return done(null, user);
            });

        }));
};

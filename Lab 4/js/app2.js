(function () {
    'use strict';

    //Skycons

    var sup;

    var skycons = new Skycons({
        "color": "#FFFAFF"
    });

    skycons.add("weather-icon", Skycons.CLEAR_DAY);

    skycons.play();

    var App = {

        APIKEY: "981becbb9b84b81bc834d357bd8554dc",
        lat: "",
        lng: "",

        init: function () {
            App.getLocation();
        },
        getLocation: function () {
            // get current position
            navigator.geolocation.getCurrentPosition(App.foundPosition);
        },
        foundPosition: function (pos) {
            //found current user position
            App.lat = pos.coords.latitude;
            App.lng = pos.coords.longitude;
            App.getWeather();
        },
        getWeather: function () {
            //get the current weather for my location

            var url = "https://api.forecast.io/forecast/" + App.APIKEY + "/" + App.lat + "," + App.lng + "?units=si";

            //JSONP
            window.jQuery.ajax({
                url: url,
                dataType: "jsonp",
                success: function (data) {
                    console.log(data);

                    localStorage.setItem("localData", JSON.stringify(data));
                    if (localStorage.getItem('temperature') !== null) {
					    temperature = JSON.parse(localStorage.getItem('temperature'));
				    }



                    if (getDate === 1 || getDate === 21 || getDate === 31) {
                        var sup = "st";
                    } else if (getDate === 2 || getDate === 22) {
                        var sup = "nd";
                    } else if (getDate === 3 || getDate === 23) {
                        var sup = "rd";
                    } else {
                        var sup = "th";

                    }

                    var date = new Date();
                    var day = date.getDay();
                    var getDate = date.getDate();
                    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saterday"];
                    var month = date.getMonth();
                    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var year = date.getFullYear();
                    var dateTomorrow = days[day + 1] + ", " + (getDate + 1) + sup + " of " + months[month] + " " + year;
                    var dateNow = days[day] + ", " + (getDate) + sup + " of " + months[month] + " " + year;

                        var weather = data.daily.data[1].summary;
                        $(".weather-summary").text(weather);

                        var weatherIcon = data.daily.data[1].icon;

                        var temperature = data.daily.data[1].apparentTemperatureMin;
                        $(".temp").text(temperature + " °c");

                        document.getElementById("date").innerHTML = dateTomorrow;



                        if (weatherIcon.indexOf("rain") >= 0) {
                            skycons.set("weather-icon", Skycons.RAIN);
                            document.getElementById("bodyColor").style.backgroundColor = "#72E1F7";

                        } else if (weatherIcon.indexOf("clear") >= 0) {

                            skycons.set("weather-icon", Skycons.CLEAR_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#FFE132";

                        } else if (weatherIcon.indexOf("cloudy") >= 0) {

                            skycons.set("weather-icon", Skycons.PARTLY_CLOUDY_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#ADADAD";

                        } else if (weatherIcon.indexOf("thunderstorm") >= 0) {
                            skycons.set("weather-icon", Skycons.SLEET);
                            document.getElementById("bodyColor").style.backgroundColor = "#2E2E2E";
                        } else if (weatherIcon.indexOf("snow") >= 0) {
                            skycons.set("weather-icon", Skycons.SNOW);
                            document.getElementById("bodyColor").style.backgroundColor = "#A6E6E1";
                        }

                    document.getElementById("weatherToday").onclick = function () {
                        var weather = data.currently.summary;
                        $(".weather-summary").text(weather);

                        var weatherIcon = data.currently.icon;

                        var temperature = data.currently.temperature;
                        $(".temp").text(temperature + " °c");

                        document.getElementById("date").innerHTML = dateNow;



                        if (weatherIcon.indexOf("rain") >= 0) {
                            skycons.set("weather-icon", Skycons.RAIN);
                            document.getElementById("bodyColor").style.backgroundColor = "#72E1F7";

                        } else if (weatherIcon.indexOf("clear") >= 0) {

                            skycons.set("weather-icon", Skycons.CLEAR_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#FFE132";

                        } else if (weatherIcon.indexOf("cloudy") >= 0) {

                            skycons.set("weather-icon", Skycons.PARTLY_CLOUDY_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#ADADAD";

                        } else if (weatherIcon.indexOf("thunderstorm") >= 0) {
                            skycons.set("weather-icon", Skycons.SLEET);
                            document.getElementById("bodyColor").style.backgroundColor = "#2E2E2E";
                        } else if (weatherIcon.indexOf("snow") >= 0) {
                            skycons.set("weather-icon", Skycons.SNOW);
                            document.getElementById("bodyColor").style.backgroundColor = "#A6E6E1";
                        }
                    }

                    document.getElementById("weatherTomorrow").onclick = function () {
                        var weather = data.daily.data[1].summary;
                        $(".weather-summary").text(weather);

                        var weatherIcon = data.daily.data[1].icon;

                        var temperature = data.daily.data[1].apparentTemperatureMin;
                        $(".temp").text(temperature + " °c");

                        document.getElementById("date").innerHTML = dateTomorrow;



                        if (weatherIcon.indexOf("rain") >= 0) {
                            skycons.set("weather-icon", Skycons.RAIN);
                            document.getElementById("bodyColor").style.backgroundColor = "#72E1F7";

                        } else if (weatherIcon.indexOf("clear") >= 0) {

                            skycons.set("weather-icon", Skycons.CLEAR_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#FFE132";

                        } else if (weatherIcon.indexOf("cloudy") >= 0) {

                            skycons.set("weather-icon", Skycons.PARTLY_CLOUDY_DAY);
                            document.getElementById("bodyColor").style.backgroundColor = "#ADADAD";

                        } else if (weatherIcon.indexOf("thunderstorm") >= 0) {
                            skycons.set("weather-icon", Skycons.SLEET);
                            document.getElementById("bodyColor").style.backgroundColor = "#2E2E2E";
                        } else if (weatherIcon.indexOf("snow") >= 0) {
                            skycons.set("weather-icon", Skycons.SNOW);
                            document.getElementById("bodyColor").style.backgroundColor = "#A6E6E1";
                        }
                    }

                }
            });

        }
    };

    App.init();

}());
